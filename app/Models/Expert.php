<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Characteristic;
use Illuminate\Support\Facades\Storage; 

class Expert extends Model
{
    use HasFactory;

    protected $fillable = [
        'nick',
        'email',
        'first_name',
        'last_name',
        'description',
        'phone',
        'avatar'
    ];

    protected static function booted()
    {
        self::deleted(function (self $model) {
            if ($model->avatar !== null) {
                Storage::disk('public')->delete($model->avatar);
            }
        });

        self::updated(function (self $model) {
            if ($model->isDirty('avatar') && $model->getOriginal('avatar') !== null) {
                Storage::disk('public')->delete($model->getOriginal('avatar'));
            }
        });        
    }

    public function characteristics()
    {
        return $this->belongsToMany(Characteristic::class, 'expert_characteristic')->withTimestamps();
    }    
}
