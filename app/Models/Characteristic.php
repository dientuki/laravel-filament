<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Characteristic extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'color'
    ];  
    
    public function experts()
    {
        return $this->belongsToMany(Expert::class, 'expert_characteristic')->withTimestamps();
    }      
}
