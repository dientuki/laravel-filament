<?php

namespace App\Filament\Resources\CharacteristicResource\Pages;

use App\Filament\Resources\CharacteristicResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateCharacteristic extends CreateRecord
{
    protected static string $resource = CharacteristicResource::class;

    public function getTitle(): string 
    {
        return __('Create Characteristic');
    }     
}
