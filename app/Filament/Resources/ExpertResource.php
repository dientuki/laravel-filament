<?php

namespace App\Filament\Resources;

use App\Filament\Resources\ExpertResource\Pages;
use App\Filament\Resources\ExpertResource\RelationManagers;
use App\Models\Expert;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Filament\Forms\Components\TextInput;
use Filament\Tables\Columns\TextColumn;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\FileUpload;

class ExpertResource extends Resource
{
    protected static ?string $model = Expert::class;

    protected static ?string $navigationIcon = 'heroicon-o-identification';

    protected static ?int $navigationSort = 10;

    public static function getLabel(): string
    {
        return trans_choice('Expert',2);
    }    

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Section::make()
                    ->schema([
                        TextInput::make('nick')
                            ->translateLabel()
                            ->required()
                            ->maxLength(2),        
                        TextInput::make('email')
                            ->translateLabel()
                            ->required()
                            ->maxLength(255)
                            ->regex('/^.+@.+$/i')
                            ->unique(),
                        TextInput::make('first_name')
                            ->translateLabel()
                            ->required()
                            ->maxLength(255),
                        TextInput::make('last_name')
                            ->translateLabel()
                            ->required()
                            ->maxLength(255),
                        Textarea::make('description')
                            ->translateLabel()
                            ->required()
                            ->columnSpan('full'),
                        Select::make('characteristics')
                            ->translateLabel()
                            ->preload()
                            ->multiple()
                            ->relationship('characteristics', 'name')    
                            ->minItems(1)                                                                               
                    ])->columnSpan(2)->columns(2),
                Section::make()
                    ->schema([
                        FileUpload::make('avatar')
                            ->translateLabel()
                            ->required()
                            ->avatar(),                            
                        TextInput::make('phone')
                            ->translateLabel()
                            ->required()
                            ->maxLength(255)                                                                          
                    ])->columnSpan(1)->columns(1),                    
            ])->columns(3);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('nick')->translateLabel(),
                TextColumn::make('email')->translateLabel()
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListExperts::route('/'),
            'create' => Pages\CreateExpert::route('/create'),
            'edit' => Pages\EditExpert::route('/{record}/edit'),
        ];
    }
}
